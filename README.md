# La grange partagée 🏡

Repo pour installer et configurer le site de la grange partagée

[![pipeline status](https://gitlab.com/la-grange-partagee/odoo-site/badges/main/pipeline.svg)](https://gitlab.com/la-grange-partagee/odoo-site/-/commits/main)

## Installation


1. create and populate a `.env` file like
`touch .env && vi .env`
the env file must be populate with:
```shell
POSTGRES_DB=xxx
ODOO_DB=xxx
POSTGRES_PASSWORD=xxx
POSTGRES_USER=xxx
PGDATA=xxx
ADMIN_PASSWORD=xxx
ODOO_DATABASE=xxx
BACKUP_URL=xxx
BACKUP_DIR=xxx
```

1. install docker https://docs.docker.com/engine/install/debian/

1. Create the necessary directories and files:
`mkdir -p nginx/certs nginx/www`

1. Comment the [nginx/conf.d/default.conf](nginx/conf.d/default.conf) server 443 section

1. Run the Nginx container temporarily to serve the webroot:
`docker run -d --name nginx-temp -v "$(pwd)/nginx/conf.d:/etc/nginx/conf.d" -v "$(pwd)/nginx/certs:/etc/nginx/certs" -v "$(pwd)/nginx/www:/var/www/html" -p 80:80 nginx`

1. up odoo and db
`docker compose up -d`

1. Run the Let's Encrypt container to obtain the SSL certificates:
`docker run -it --rm --name certbot-tmp -v "$(pwd)/nginx/certs:/etc/letsencrypt" -v "$(pwd)/nginx/www:/var/www/html" certbot/certbot certonly --webroot --webroot-path=/var/www/html --email bureau@la-grange-partagee.fr --agree-tos --no-eff-email -d la-grange-partagee.fr`

1. Stop and remove the temporary Nginx container:
`docker stop nginx-temp && docker rm nginx-temp`

1. Un-comment the [nginx/conf.d/default.conf](nginx/conf.d/default.conf) server 443 section

1. Run the following command to start all services defined in your docker-compose.yml file
`docker compose restart`


## Usage

https://la-grange-partagee.fr


## Backup

### DB + File store

`docker compose build  backup`
`docker compose run --rm backup`

### Backup the backup folder

1. Install Duplicity

```shell
sudo apt update
sudo apt install duplicity
```

2. Set Up SSH Key Authentication

Generate an SSH key pair (if not already set up)
```shell
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

Copy the public key to the remote server
```shell
ssh-copy-id user@remote_host
```

Test SSH Connection: Verify you can log in without a password
```shell
ssh user@remote_host
```

3. Run a Manual Backup

Use the duplicity command to back up a folder to the remote server
```shell
duplicity /path/to/local/folder ssh://user@remote_host//path/to/remote/folder
```


4. Automate Backups with Cron
```shell
crontab -e
```

Eg add the following line to schedule daily backups at 2:00 AM
```shell
0 2 * * * /usr/bin/duplicity /path/to/local/folder ssh://user@remote_host//path/to/remote/folder >> /var/log/duplicity.log 2>&1
```

Save and exit the crontab


6. Verify Backups
```shell
duplicity verify ssh://user@remote_host//path/to/remote/folder /path/to/local/folder
```
```shell

```

7. Restore from Backup
```shell
duplicity restore ssh://user@remote_host//path/to/remote/folder /path/to/restore/location
```
