import unittest
from unittest.mock import patch, MagicMock
from environs import Env
import os
from backup_scripts.backup import get_env_config

class TestGetEnvConfig(unittest.TestCase):

    @patch.dict(os.environ, {"ADMIN_PASSWORD": "test_password", "BACKUP_DIR": "/test/backup", "ODOO_DATABASE": "y", "BACKUP_URL": "x"})
    @patch('backup_scripts.backup.env')
    def test_env_vars_set(self, mock_env):
        # Mock the .env file reading
        mock_env.return_value = Env()
        mock_env.return_value.read_env = MagicMock()

        env_vars = ["ADMIN_PASSWORD", "BACKUP_DIR", "ODOO_DATABASE", "BACKUP_URL"]
        result = get_env_config(env_vars)

        expected_result = {
            "ADMIN_PASSWORD": "test_password",
            "BACKUP_DIR": "/test/backup",
            "ODOO_DATABASE": "y",
            "BACKUP_URL": "x",
        }
        self.assertEqual(result, expected_result)

    @patch.dict(os.environ, {"X": "None"})
    @patch('backup_scripts.backup.env')
    def test_env_vars_not_set(self, mock_env):
        # Mock the .env file reading
        mock_env_instance = MagicMock()
        mock_env.return_value = mock_env_instance
        mock_env_instance.read_env = MagicMock()
        mock_env_instance.side_effect = lambda var: f"default_{var}"

        env_vars = ["BACKUP_DIR"]
        result = get_env_config(env_vars)

        expected_result = {
            "BACKUP_DIR": "/backups",
        }
        self.assertEqual(result, expected_result)

    @patch.dict(os.environ, {"ADMIN_PASSWORD": "test_password"})
    @patch('backup_scripts.backup.env')
    def test_mixed_env_vars(self, mock_env):
        # Mock the .env file reading
        mock_env_instance = MagicMock()
        mock_env.return_value = mock_env_instance
        mock_env_instance.read_env = MagicMock()
        mock_env_instance.side_effect = lambda var: f"default_{var}" if var != "ADMIN_PASSWORD" else "test_password"

        env_vars = ["ADMIN_PASSWORD", "BACKUP_DIR"]
        result = get_env_config(env_vars)

        expected_result = {
            "ADMIN_PASSWORD": "test_password",
            "BACKUP_DIR": "/backups",
        }
        self.assertEqual(result, expected_result)
