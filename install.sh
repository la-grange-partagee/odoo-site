echo update
sudo apt update -y
echo upgrade
sudo apt upgrade -y
echo "\n"


echo vim
sudo apt install vim -y
echo "\n"


echo zsh
sudo apt install zsh -y
echo "\n"

echo ohmyzsh
sudo sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
echo "\n"
