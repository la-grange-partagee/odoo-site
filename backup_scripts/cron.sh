#!/bin/bash

echo "run cron 🔁"

# Add a cron job to run the backup script daily at 2am
cron && tail -f /var/log/cron.log
