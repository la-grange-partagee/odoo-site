import os
import requests
from datetime import datetime, timedelta
import glob
import logging
from environs import Env
from urllib.parse import urlparse


# Configure logging
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(message)s",
)

env_vars = [
    "ADMIN_PASSWORD",
    "BACKUP_DIR",
    "ODOO_DATABASE",
    "BACKUP_URL",
]

def get_env_config(env_vars):
    # Load environment variables
    env = Env()
    env.read_env()  # Read the .env file

    env_configs = {}

    for env_var in env_vars:
        env_configs[env_var] = os.getenv(env_var, False)
        if not env_configs[env_var]:
            env_configs[env_var] = env(env_var) # Load from .env file

    return env_configs

def make_new_backup(env_configs):
    # Create a backup directory
    os.makedirs(env_configs.get("BACKUP_DIR"), exist_ok=True)

    data = {
        "master_pwd": env_configs.get("ADMIN_PASSWORD"),
        "name": env_configs.get("ODOO_DATABASE"),
        "backup_format": "zip"
    }

    domain = urlparse(env_configs.get("BACKUP_URL")).netloc

    # Define the output file name
    backup_file = os.path.join(env_configs.get("BACKUP_DIR"), f"{domain}_{env_configs.get("ODOO_DATABASE")}.{datetime.now().strftime('%Y-%m-%d')}.zip")
    logging.info(f"🏁 Start to do backup for {backup_file}")


    # Make the POST request
    response = requests.post(env_configs.get("BACKUP_URL"), data=data)

    # Save the response content to a file
    if response.status_code == 200:
        with open(backup_file, 'wb') as f:
            f.write(response.content)
        logging.info(f"Backup saved as {backup_file}")
    else:
        logging.error(f"Failed to download backup: {response.status_code} - {response.text}")

    os.chmod(env_configs.get("BACKUP_DIR"), 0o777)
    return domain


def delete_old_backups(domain, env_configs):
    # Delete old backups
    cutoff_date = datetime.now() - timedelta(days=7)
    for backup_path in glob.glob(os.path.join(env_configs.get("BACKUP_DIR"), f"{domain}_{env_configs.get("ODOO_DATABASE")}.*.zip")):
        if os.path.getmtime(backup_path) < cutoff_date.timestamp():
            os.remove(backup_path)
            logging.info("Deleted old backup: %s", backup_path)

if __name__ == "__main__":
    env_config = get_env_config(env_vars=env_vars)
    domain = make_new_backup(env_configs=env_config)
    delete_old_backups(domain=domain, env_configs=env_config)
