import os
from datetime import datetime

path_to_file = "backups/la-grange-partagee.fr_odoo.2024-11-20.zip"
date_time = datetime.strptime("2024-11-20", "%Y-%m-%d")

access_time = date_time.timestamp()
modification_time = date_time.timestamp()

print(date_time)
print(modification_time)

os.utime(path_to_file, (access_time, modification_time))
