#!/bin/bash

echo "run backups 💽"

# Create a backup directory if it doesn't exist
mkdir -p /backups

# Create the .pgpass file
echo "db:5432:${ODOO_DB}:${POSTGRES_USER}:${POSTGRES_PASSWORD}" > ~/.pgpass
chmod 600 ~/.pgpass

# Backup filename with date
BACKUP_FILE="/backups/backup_$(date +%Y%m%d%H%M%S).sql"

# Wait for the database to be ready
until pg_isready -h db -U $POSTGRES_USER; do
  echo "Waiting for database to be ready..."
  sleep 2
done

# Run the pg_dump command
pg_dump -h db -U $POSTGRES_USER -d $ODOO_DB > $BACKUP_FILE

# Remove backups older than 10 days
find /backups -type f -name "*.sql" -mtime +10 -exec rm -f {} \;

echo "backup done ✅"
